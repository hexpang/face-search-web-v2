import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import AutoImport from 'unplugin-auto-import/vite';
import Components from 'unplugin-vue-components/vite';
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers';
import path from "node:path";
import Icons from "unplugin-icons/vite";
import IconsResolver from "unplugin-icons/resolver";
import VueMacros from "unplugin-vue-macros/vite";
import Pages from "vite-plugin-pages";

const timeStamp = new Date().getTime();

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    VueMacros({
      plugins: {
        vue: vue({
          reactivityTransform: true,
        }),
      },
    }),
    Pages(),
    Icons(),
    AutoImport({
      imports: ['vue']
    }),
    Components({
      resolvers: [NaiveUiResolver(), IconsResolver({
        prefix: "icon",
      })]
    })
  ],
  resolve: {
    alias: {
      "@": path.resolve(__dirname, "src"),
      "@component": path.resolve(__dirname, "src/components"),
      "@store": path.resolve(__dirname, "src/store"),
      "@plugin": path.resolve(__dirname, "src/plugins"),
      "@api": path.resolve(__dirname, "src/api"),
      "@util": path.resolve(__dirname, "src/utils"),
    },
    extensions: ['.js', '.vue', '.json'],
  },
  // 开发时候的端口号，默认为3000
  server: {
    port: 3000,
    proxy: {
      "/api": {
        target: "http://10.0.0.15:56789",
        changeOrigin: true,
        rewrite: (p) => p.replace(/^\/api/, ""),
      },
    }
  },
  // 打包配置
  build: {
    // 打包文件夹名称
    outDir: 'dist',
    // 打包后去掉console语句
    terserOptions: {
      compress: {
        drop_console: true
      }
    },
    compress: {
      drop_console: true,
      drop_debugger: true
    },
    // 打包后的文件名
    rollupOptions: {
      output: {
        entryFileNames: `assets/[name].${timeStamp}.js`,
        chunkFileNames: `assets/[name].${timeStamp}.js`,
        assetFileNames: `assets/[name].${timeStamp}.[ext]`
      }
    }
  }
})
