// 公共复制剪贴板
const uiCopy = str => {
    let copyDom = document.createElement('div');
    copyDom.innerText = str;
    copyDom.style.position = 'absolute';
    copyDom.style.top = '0px';
    copyDom.style.right = '-9999px';
    document.body.appendChild(copyDom);
    //创建选中范围
    let range = document.createRange();
    range.selectNode(copyDom);
    //移除剪切板中内容
    window.getSelection().removeAllRanges();
    //添加新的内容到剪切板
    window.getSelection().addRange(range);
    //复制
    let successful = document.execCommand('copy');
    copyDom.parentNode.removeChild(copyDom);
    try {
        uiMsg(successful ? '复制成功!' : '复制失败，请手动复制内容', null, successful ? 'success' : 'error');
    } catch (err) { }
};

// 下载base64格式的图片
const uiDownloadImg = (data, fileName) => {
    var link = document.createElement('a');
    link.href = data;
    link.download = fileName;
    link.click();
};

const BlobToDataURL = (blob, cb) => {
    let reader = new FileReader();
    reader.onload = function (evt) {
        let base64 = evt.target.result;
        cb(base64);
    };
    reader.readAsDataURL(blob);
};

export { uiCopy, uiDownloadImg, BlobToDataURL };
