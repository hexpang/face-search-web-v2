export type DataColumnType = {
    comment: String,
    dataType: String,
    name: String,
};

export type CollectionType = {
    collectionComment: String,
    collectionName: String,
    faceColumns: DataColumnType[],
    namespace: String,
    replicasNum: Number,
    sampleColumns: DataColumnType[],
    shardsNum: Number,
    storageEngine: String,
    storageFaceInfo: Boolean,
}
