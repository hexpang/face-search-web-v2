import { createApp } from 'vue';
import App from './App.vue';
import './style/index.css';
import { uiCopy, uiDownloadImg } from './utils';
import request from "@plugin/axios";
import moment from 'moment';
import pinia from './plugins/pinia';
import router from './plugins/router';

const app = createApp(App);

app.use(pinia);
app.use(router);

if (import.meta.env.MODE === 'production') {
    app.config.devtools = false;
} else {
    app.config.devtools = true;
}

// 请求队列
window.__axiosPromiseArr = [];

// 挂载全局变量
app.config.globalProperties.$request = request;
app.config.globalProperties.$moment = moment;
app.config.globalProperties.$uiCopy = uiCopy;
app.config.globalProperties.$uiDownloadImg = uiDownloadImg;

app.mount('#app');
