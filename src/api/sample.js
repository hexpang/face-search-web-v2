import request from "@plugin/axios";
import { useSystemStore } from "@store/system";
const system = useSystemStore();

/**
 * 获取样本数据
 * @param page
 * @param pageSize
 * @returns {*}
 */
export function listSample(page = 1, pageSize = 15) {
    return request.get("/visual/sample/list", {
        params: {
            namespace: system.namespace,
            collectionName: system.collection,
            offset: page * pageSize - pageSize,
            limit: pageSize,
        }
    })
}

/**
 * 创建样本
 * @param data
 * @returns {*|Promise<AxiosResponse<any>>}
 */
export function createSample(data) {
    return request.post("/visual/sample/create", data);
}

/**
 * 删除样本数据
 * @param namespace
 * @param collectionName
 * @param sampleId
 * @returns {*}
 */
export function deleteSample(namespace, collectionName, sampleId) {
    return request.get("/visual/sample/delete", {
        params: { namespace, collectionName, sampleId }
    });
}
