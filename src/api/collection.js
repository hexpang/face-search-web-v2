import request from "@plugin/axios";
import { useSystemStore } from "@store/system";

/**
 * 根据命名空间查看集合列表
 * @returns {*}
 */
export function listCollection () {
    return request.get("/visual/collect/list", {
        params: {
            namespace: useSystemStore().namespace,
        }
    });
}

/**
 * 创建一个集合(数据库)
 * @param data
 * @returns {*|Promise<AxiosResponse<any>>}
 */
export function createCollection (data) {
    return request.post("/visual/collect/create", data);
}

/**
 * 删除集合
 * @param namespace
 * @param collectionName
 */
export function deleteCollection (namespace, collectionName) {
    return request.get("/visual/collect/delete", {
        params: { namespace, collectionName }
    });
}
