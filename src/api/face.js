import request from "@plugin/axios";
import { useSystemStore } from "@store/system";
const system = useSystemStore();

/**
 * 创建人脸数据
 * @param data
 * @returns {*|Promise<AxiosResponse<any>>}
 */
export function createFace(data) {
    return request.post("/visual/face/create", data);
}
