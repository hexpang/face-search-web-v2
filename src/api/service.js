import request from "@plugin/axios";
import { useSystemStore } from "@store/system";
const system = useSystemStore();

/**
 * 1vN搜索
 * @param data
 * @returns {*|Promise<AxiosResponse<any>>}
 */
export function searchService (data) {
    return request.post("/visual/search/do", data);
}
