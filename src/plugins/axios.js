import axios from 'axios';
import pinia from './pinia';
import { useUserStore } from '../store/user';

const user = useUserStore(pinia);

const apiHost = '/api'; // 接口地址

const request = axios.create({
    baseURL: import.meta.env.MODE === 'production' ? import.meta.env.VITE_API_BASEURL : apiHost,
    timeout: 60000
});

export default request;
