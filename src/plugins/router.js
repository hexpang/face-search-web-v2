import { createRouter, createWebHistory } from 'vue-router';
import routes from "virtual:generated-pages";

const router = createRouter({
    history: createWebHistory('/'),
    routes: routes
});

router.afterEach(to => {
    window.scrollTo(0, 0);
});
window['$router'] = router;

export default router;
