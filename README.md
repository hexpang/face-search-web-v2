# face-search-web-v2

## 说明

基于 vite.js + naive-ui 实现，face-search 版本为 **2.1.0**，实现了部分接口功能，简化了样本创建流程（可以批量上传照片）。通过在顶部输入命名空间进行切换。

再次感谢 [divenswu](https://gitee.com/divenswu)

## 截图

![ScreenShot](./screenshot/1.png)
![ScreenShot](./screenshot/2.png)
![ScreenShot](./screenshot/3.png)
![ScreenShot](./screenshot/4.png)
![ScreenShot](./screenshot/5.png)

## Face Search

[https://gitee.com/open-visual/face-search/](https://gitee.com/open-visual/face-search/)

## 安装
```
pnpm install
```

### 启动
```
pnpm dev
```

### 打包
```
pnpm build
```
